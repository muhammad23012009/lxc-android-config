# shellcheck shell=sh

# Copyright (C) 2021-2022 UBports Foundation.
# SPDX-License-Identifier: GPL-3.0-or-later

# Grabbed from Debian's initramfs-tools (GPL-2.0-or-later).
# Resolve device node from a name.  This expands any LABEL or UUID.
# $1=name
# Resolved name is echoed.
resolve_device() {
    DEV="$1"

    case "$DEV" in
    LABEL=* | UUID=* | PARTLABEL=* | PARTUUID=*)
        DEV="$(blkid -l -t "$DEV" -o device)" || return 1
        ;;
    esac
    [ -e "$DEV" ] && echo "$DEV"
}

ab_slot_detect_done=""

find_partition_path() {
    # Note that we run early before udev coldboot, so if we boot without initrd,
    # /dev/disk/by-* might not be available. blkid (and resolve_device) can work
    # without them in a number of cases, and thus LABEL=* form is used for those.

    if [ -z "$ab_slot_detect_done" ]; then
        ab_slot_suffix=$(grep -o 'androidboot\.slot_suffix=..' /proc/cmdline |  cut -d "=" -f2)
        if [ -n "$ab_slot_suffix" ]; then
            echo "Detected slot suffix $ab_slot_suffix" >&2
        fi
        ab_slot_detect_done=1
    fi

    for detection in \
            PARTLABEL= \
            /dev/disk/by-name/ \
            LABEL= \
            /dev/mapper/ \
            /dev/disk/by-path/ \
            UUID= \
            PARTUUID= \
            /dev/disk/by-id/ \
    ; do
        if resolve_device "${detection}${1}"; then
            break
        fi

        if [ -n "$ab_slot_suffix" ] && \
                resolve_device "${detection}${1}${ab_slot_suffix}"; then
            break
        fi
    done
}
