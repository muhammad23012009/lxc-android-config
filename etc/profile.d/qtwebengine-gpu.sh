#!/bin/sh
# This file is part of lxc-android-config

# Set some baseline performance optimizations
GPU_FEATURES="--enable-gpu-rasterization"

# No default multimedia capabilities by default
MEDIA_FEATURES=""

if [ -f /system/build.prop ]; then
    # Disable GPU compositing for QtWebEngine on Android 7.1 and older devices
    SDK_VERSION=$(getprop ro.build.version.sdk)
    if test "$SDK_VERSION" -lt 28; then
        GPU_FEATURES="${GPU_FEATURES} --disable-gpu-compositing"
    fi

    # Enable OMX video decoding capabilities using Mojo
    MEDIA_FEATURES="--enable-accelerated-video-decode --enable-features=MojoVideoDecoder"
fi

export QTWEBENGINE_CHROMIUM_FLAGS="$GPU_FEATURES $MEDIA_FEATURES"
